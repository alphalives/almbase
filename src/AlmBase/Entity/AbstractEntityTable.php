<?php
namespace AlmBase\Entity;

use Doctrine\DBAL\LockMode;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\Common\Collections\Selectable;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\ExpressionBuilder;
use Doctrine\ORM\EntityRepository;

abstract class AbstractEntityTable extends EntityRepository
{

    /**
     * Chemin de la classe
     * 
     * @var unknown
     */
    protected $entityPath;

    /**
     * Diminutif donné à la table utilisé dans les requêtes
     * 
     * @var unknown
     */
    protected $diminutifTable;

    protected $qb;

    protected function dataTableJoin()
    {
    }
    
    
    public function findDataTable($colonne, $param)
    {
        $this->qb = $this->_em->createQueryBuilder($this->diminutifTable);
        $this->qb->select($this->diminutifTable)->from($this->entityPath, $this->diminutifTable);
        $this->dataTableJoin();
        
        $this->getDataTableQuery($colonne, $param);
        $this->qb->setFirstResult($param['iDisplayStart']);
        $this->qb->setMaxResults($param['iDisplayLength']);
        return $this->qb->getQuery()->getResult();
    }

    public function countDataTable($colonne, $param)
    {
        $this->qb = $this->_em->createQueryBuilder('a');
        $this->qb->select('COUNT (' . $this->diminutifTable . '.id) AS nb')->from($this->entityPath, $this->diminutifTable);
        $this->dataTableJoin();
        $this->getDataTableQuery($colonne, $param);
        $r = $this->qb->getQuery()->getOneOrNullResult();
        return $r["nb"];
        
        //$r = new Paginator($qb-> getQuery(), $fetchJoin = true);
		//return $r -> count();
    }
    
    public function countTotalDataTable($colonne, $param)
    {
        $this->qb = $this->_em->createQueryBuilder('a');
        $this->qb->select('COUNT (' . $this->diminutifTable . '.id) AS nb')->from($this->entityPath, $this->diminutifTable);
        $this->dataTableJoin();
        $r = $this->qb->getQuery()->getOneOrNullResult();
        return $r["nb"];
    }

    public function getDataTableQuery($colonne, $param)
    {
        if ($param['sSearch'] != '') {
            $s = ' 1=2 ';
            foreach ($colonne as $cols) {
                if ($cols['searchable'] == true) {
                    $s .= ' OR '.$cols['id'] . ' LIKE :sSearch ';
                }
            }
            $this->qb->andWhere($s)->setParameter('sSearch', '%' . $param['sSearch'] . '%');
        }
        
        $i=0;
        foreach($colonne as $s){
            if($param['tabSearchCols'][$i] != ''){
                 $this->qb->andWhere($s['id']. ' LIKE :sSearch_'.$i)->setParameter('sSearch_'.$i, '%' . $param['tabSearchCols'][$i] . '%');
            }
            $i++;
        }
        
        for ($i = 0; $i < intval($param['iSortingCols']); $i ++) {
            if ($colonne[$param['tabSortCols'][$i]['id']]['sortable'] == true) {
                $this->qb->addOrderBy($colonne[$param['tabSortCols'][$i]['id']]['id'], strtoupper($param['tabSortCols'][$i]['dir']));
            }
        }
        
        return $this;
    }
}
