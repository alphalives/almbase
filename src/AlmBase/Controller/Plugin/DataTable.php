<?php

namespace AlmBase\Controller\Plugin;

use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\View\Model\ViewModel as ViewModel;

class DataTable {
	
	protected $colonne = array();
	
	//Options
	protected $numColonneTrieeParDefaut = 0;
	protected $sensTriParDefaut = 'desc';
	protected $urlLstTable;
	protected $afficheRecherche = true;
	protected $afficheExport = true;
	protected $tabBouton = array();
	protected $tabActionCheckbox = array();
	protected $param = array();
	
	
	public function __construct($colonne)
	{
		$this -> colonne = $colonne;
	}
	
	/**
	* Magic getter to expose protected properties.
	*
	* @param string $property
	* @return mixed
	*/
    public function __get($property)
    {
        return $this->$property;
    }
	
	/**
	* Magic setter to save protected properties.
	*
	* @param string $property
	* @param mixed $value
	*/
    public function __set($property, $value)
    {
        $this->$property = $value;
    }
		
	
	public function setParamListe($param)
	{
		$tabParam = array();
		
		$tabParam['sEcho'] = (int) $param->fromQuery('sEcho');
		$tabParam['iColumns'] = (int) $param->fromQuery('iColumns');
		$tabParam['sSearch'] = $param->fromQuery('sSearch');
		$tabParam['iDisplayStart'] = (int) $param->fromQuery('iDisplayStart');
		$tabParam['iDisplayLength'] = (int) $param->fromQuery('iDisplayLength');
		
		$tabParam['iSortingCols'] = (int) $param->fromQuery('iSortingCols');
		$tabSortCols = array();
		for ( $i=0 ; $i<intval( $tabParam['iSortingCols'] ) ; $i++ ){
			$tabSortCols[$i]['id'] = $param->fromQuery('iSortCol_'.$i);
			$tabSortCols[$i]['dir'] = $param->fromQuery('sSortDir_'.$i);
		}
		$tabParam['tabSortCols'] = $tabSortCols;
		
		$tabSearchCols = array();
		for ( $i=0 ; $i<intval( $tabParam['iColumns'] ) ; $i++ ){
		    $tabSearchCols[$i] = $param->fromQuery('sSearch_'.$i);
		}
		$tabParam['tabSearchCols'] = $tabSearchCols;
		
		return $tabParam;	
	}
	
	
	protected function _creerParamColonne($colonne)
	{
		$s = '{';
		if(isset ($colonne['sortable']) && $colonne['sortable'] == true) 	$s .= '"bSortable" : true,'; else $s .= '"bSortable" : false,'; 
		if(isset ($colonne['searchable']) && $colonne['searchable']) 		$s .= '"bSearchable" : true,'; else $s .= '"bSearchable" : false,';
		if(isset ($colonne['sType'])) 	$s .= '"sType" : "'.$colonne['sType'].'",';
		 
		$s .= '"sClass" : "';
		if(isset ($colonne['class']) && $colonne['class'] != '') $s .= ''.$colonne['class'].'';
		if(isset ($colonne['cliquable']) && $colonne['cliquable'] == true) $s .= ' tdCliquable';
		$s .= '",'; 
				
		$s = substr($s,0,-1);
		$s .= '}';	
		return $s;
	}
	
	
	
	public function genererJs()
	{	
		ob_start();
		?>
<script src="/alm-admin/ace/js/jquery.dataTables.min.js"></script>
<script src="/alm-admin/ace/js/jquery.dataTables.bootstrap.js"></script>
<script src="/alm-admin/js/dataTables.individualColumnFiltering.js"></script>

<script type="text/javascript">
        var oTable;
        $(document).ready(function() {
				var charge = true;
                oTable = $('.tblLst').dataTable( {  
                
				/* Persistance de la configuation */
						"bStateSave": true,
						"bRetrieve": true,
						
				"bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "<?php echo $this->urlLstTable; ?>",
                "bLengthChange": false,
                "bScrollInfinite": false,
                "bScrollCollapse": true,
                //"sScrollY": "500px",
                //"sDom": '<"top">rt<"bottom"p><"clear">', 
                "bPaginate": true,
				"sPaginationType": "full_numbers",
                "iDisplayLength" : 20,
                
				"aoColumns": [
					<? $i=0; foreach($this -> colonne as $key =>  $var){ $i++; 
						if($i < count($this -> colonne)) echo $this -> _creerParamColonne($var).','; 
						else echo $this -> _creerParamColonne($var);
					} ?>
				],

				"bSort": true,
				
				<? if(isset($this -> numColonneTrieeParDefaut) && isset($this -> sensTriParDefaut)) { ?>
					"aaSorting": [ [<?= $this -> numColonneTrieeParDefaut ?>,'<?= $this -> sensTriParDefaut ?>'] ],
				<? } ?>
                
                "fnServerData": function ( sSource, aoData, fnCallback ) {
					<? if(is_array($this -> param) && count($this -> param) > 0) { 
						foreach($this -> param as $key => $var) { ?>
							aoData.push( { "name": "<?= $key ?>", "value": "<?= $var ?>" } );
						<? }
					} ?>
					$.getJSON( sSource, aoData, function (json) { 
						fnCallback(json);
						//Recherche
						var flag = false;
						var rech = '';
						$.each(aoData, function(index, value) {
						  $.each(value, function(ind, val) {
								if(val == 'sSearch'){
									flag = true;
								}
								if(flag && val != 'sSearch'){
									rech = val;
									flag = false;
								}
						  });
						});
						if(charge == true){
							$('#rechercheBouton').val(rech);
						}
						charge = false;
						
						$('.tblLst td.tdCliquable').click(function(){
							td = this.parentNode.childNodes;
							nbTd = td.length;
							for(i = 0; i < nbTd; i++)	{
								a = td[i].childNodes
								for(j = 0; j < a.length; j++)	{
									collection = a[j].attributes;
									if(collection){
										for(var k=0; k<collection.length; k++)
										{
										  if(collection[k].value == 'lstEdit'){
											  for(var l=0; l<collection.length; l++){
												  if(collection[l].name == 'href') monHref = collection[l].value ;
											  }
										  }
										}
									}
								}
								
							}
							$(location).attr('href',monHref);
						});	
					});
				}
            });



              

                
          });
            
        function fnFilterGlobal ()
        {
            $('.tblLst').dataTable().fnFilter(
                $("#global_filter").val(),
                null,
                $("#global_regex")[0].checked,
                $("#global_smart")[0].checked
            );
        }
         
        function fnFilterColumn ( i )
        {
            $('.tblLst').dataTable().fnFilter(
                $("#col"+(i+1)+"_filter").val(),
                i, false, false
               // $("#col"+(i+1)+"_regex")[0].checked,
                //$("#col"+(i+1)+"_smart")[0].checked
            );
        }
        
        function rechercher()
		{
            var recherche = $('#rechercheBouton').val();
            oTable.fnFilter(recherche);	
        }
		
		
		function selectCheckBox(checkAll)
		{
			var flagCoche = false
			$('input[type=checkbox][class=check]').each(function() {
				if($(this).attr('checked')) {
					flagCoche = true;
				}		
			});

			if(checkAll == true) flagCoche = !flagCoche;
			
			if(flagCoche == true){ 
				$('#fauxselect5').css({'display': 'inline'});
				$('#labelToutCoche').html('Tout d�cocher');
			}else{
				$('#toutCoche').attr('checked', false);
				$('#fauxselect5').css({'display': 'none'});
				$('#labelToutCoche').html('Tout cocher');
			}  
			if(checkAll == true){
				$(".check").each( 
					function() { 
					   $(this).attr('checked', flagCoche); 
					} 
				);
			}
			
		}
		
		function supprimer(lien){
			if(confirm('Voulez-vous vraiment supprimer tous les �l�ments coch�s ?')){	
		
				var tabSup=new Array;
				$('input[type=checkbox][class=check]:checked').each(function() {
					var idCase = $(this).attr('id');
					idCase = idCase.split('-');
					tabSup[idCase[1]] = idCase[1];
				});

				$.ajax({
					type: 'POST',
					url: lien,
					data: 'tabSup='+tabSup,
					success: function(data) {
						document.location.reload();
					}
				});
			
			}	
		}
        
        </script>


<?
		$content = ob_get_contents ();
		ob_end_clean();
		return $content;
	}
	
	public function genererJsBouton()
	{	
		ob_start();
		?>
<script type="text/javascript"
	src="/alm-admin/ace/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="/alm-admin/ace/js/jquery.dataTables.bootstrap.js"></script>
<script type="text/javascript">
				
				$(document).ready(function() {
					
					// On cr�� le timer
					function setInactiveTimer(toto){
						$(toto).data(
							'inactiveTimer',
							setTimeout( function(){fermeMenu(toto)}, 200 )
						);
					}
					
					// On d�sactive le timer 
					function clearIntactiveTimer(tata){
						clearTimeout($(tata).data('inactiveTimer'));
					}
					
					// On ferme le menu
					function fermeMenu(toto){
						$(".fauxselect").removeClass('ouvert');
						$(toto).slideUp("fast");
					}
					
					// Cache les sous menu
					$(".fauxselectcontenu").hide();
					
					// Gestion des ouvertures
					$(".fauxselect").click(function(){
						$(".fauxselectcontenu").hide();
						$(this).addClass('ouvert');
						var ide = $(this).attr('id');
						var position = $(this).offset();
						$("."+ide).css({ "left": (position.left-198) + "px", "top": (position.top-10) + "px" });
						$("."+ide).slideDown('fast');
						
					}).mouseleave(function(){
						var ide = $(this).attr('id');
						var tete = $("."+ide);
						setInactiveTimer(tete);
					});
					
					$(".fauxselectcontenu").mouseleave(function(){
						$(".fauxselect").removeClass('ouvert');
						$(this).slideUp("fast");
					}).mouseenter(function(){
						clearIntactiveTimer(this);	
					});
					
					
				});
		</script>
<?
		$content = ob_get_contents ();
		ob_end_clean();
		return $content;
	}
	
	
	
	
	public function genererHtml()
	{
		ob_start();
		?>
<form id="form_lst" action="" method="post"
	enctype="multipart/form-data" class="formulaire">
	<input type="hidden" name="action" value="ordre" />
	<table id="tblLst"
		class="tblLst table table-striped table-bordered table-hover">
		<thead>
			<tr>
           <? foreach($this -> colonne as $key =>  $var){ ?>
            <th
					<? if(isset($var['width'])) echo 'width="'.$var['width'].'"' ?>><?= $var['name'] ?></th>
           <? } ?>
        </tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="<?= count($this -> colonne) ?>">Chargement ...</td>
			</tr>
		</tbody>
		
	</table>
</form>
<?
		$content = ob_get_contents ();
		ob_end_clean();
		return $content;
	}
	
	
	
	public function genererBouton()
	{
		ob_start();
		?>
<div id="enteteDroite">
            	<? if($this -> afficheRecherche == true) { ?>
                    <div class="outillageFormDroite">
		<form id="form_recherche" name="form_recherche" action="" method=""
			onsubmit="return false">
			<input type="text" id="rechercheBouton" name="rechercheBouton"
				onKeyUp="javascript:rechercher()" placeholder="Rechercher..." />
		</form>
	</div>  
                <? } ?> 
                <div id="bandeauBouton" class="outillageForm">
                	<? if(count($this -> tabActionCheckbox) > 0) { ?>
                    	<a class="fauxselect secondaire fleche"
			id="fauxselect1"><span class="icoChoix"></span> S�lectionner</a> <a
			class="fauxselect secondaire fleche" id="fauxselect5"
			style="display: none;"><span class="icoOutil"></span> Action</a>   
                    <? } ?>
                   
                    <? foreach($this -> tabBouton as $key => $var) { ?>                  
                    	<a class="fauxselect clignotte" id=""
			href="<?= $var['lien'] ?>"><span
			<?php /*?>class="icoExporter"<?php */?>></span> <?= $var['libelle'] ?></a>
                	<? } ?>
                </div>

	<div class="cale"></div>
</div>

<? if(count($this -> tabActionCheckbox) > 0) { ?>
<div class="fauxselectcontenu fauxselect1">
	<span class="pointe"></span>
	<form id="form_toutCoche" name="form_toutCoche" action="" method="">
		<a href="#" id="labelToutCoche" onClick="javascript:selectCheckBox(1)"
			style="color: #FFF;">Tout cocher</a>
	</form>
</div>

<div class="fauxselectcontenu fauxselect5">
	<span class="pointe"></span>
                    <? foreach($this -> tabActionCheckbox as $key => $var) { ?> 
                    	<a href="<?= $var['lien'] ?>" style="color: #FFF;"
		<? if($var['onclick'] != '') { ?>
		onclick="javascript:<?= $var['onclick'] ?>" <? } ?>><img alt="sup"
		src="<?= $var['icone'] ?>" /> <?= $var['libelle'] ?></a>
                    <? } ?>
                </div>
<? } ?>
		<?
		$content = ob_get_contents ();
		ob_end_clean();
		return $content;
	}
	
	 
}