<?php

namespace Pronoparty\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\ServiceLocatorInterface;
use ZfcUser\Authentication\Adapter\AdapterChain as AuthAdapter;

class Group extends AbstractPlugin implements ServiceManagerAwareInterface
{
    public function __invoke()
    {
        $config = $this->serviceManager->get('Config');
        
        switch($config['app']['mode']){
        	case 'prod' : 
        	    return 'prod';
        	    break;
            
        	case 'dev' :
        	    return 'dev' ;
        	    break;

        	case 'preprod' :
        	    throw new \Exception("to do");
        	    break;
        	    
            default : 
                throw new \Exception("Mode inconnu");
        }
        
    }
    
    /**
     * Set service manager instance
     *
     * @param ServiceManager $serviceManager
     * @return User
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

}
