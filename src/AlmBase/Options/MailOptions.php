<?php

namespace AlmBase\Options;

use Zend\Stdlib\AbstractOptions;

class MailOptions extends AbstractOptions
{
    protected $type = '';
    
   	protected $envoiFromName = '';
    protected $envoiMail = '';
    protected $envoiReplyTo = '';
	
	protected $envoiName = '';
    protected $envoiHost = '';
    protected $envoiConnectionClass = '';
    protected $envoiPort = '';
    protected $envoiSsl = '';
    protected $envoiUsername = '';
    protected $envoiPassword = '';
	protected $mailAdministrateur = '';
    
	public function setType($var) {
	    $this->type = $var;
	}
	
	 public function setEnvoiFromName($var) {
        $this->envoiFromName = $var;
    }
	
    public function setEnvoiMail($envoiMail) {
        $this->envoiMail = $envoiMail;
    }

    public function setEnvoiReplyTo($envoiReplyTo) {
        $this->envoiReplyTo = $envoiReplyTo;
    }

	public function setEnvoiName($var) {
        $this->envoiName = $var;
    }
	
    public function setEnvoiHost($envoiHost) {
        $this->envoiHost = $envoiHost;
    }

    public function setEnvoiConnectionClass($envoiConnectionClass) {
        $this->envoiConnectionClass = $envoiConnectionClass;
    }

    public function setEnvoiPort($envoiPort) {
        $this->envoiPort = $envoiPort;
    }

    public function setEnvoiSsl($envoiSsl) {
        $this->envoiSsl = $envoiSsl;
    }

    public function setEnvoiUsername($envoiUsername) {
        $this->envoiUsername = $envoiUsername;
    }

    public function setEnvoiPassword($envoiPassword) {
        $this->envoiPassword = $envoiPassword;
    }
	
	public function setMailAdministrateur($var) {
        $this->mailAdministrateur = $var;
    }
	
    public function getType() {
        return $this->type;
    }
    
 	public function getEnvoiFromName() {
        return $this->envoiFromName;
    }
	
    public function getEnvoiMail() {
        return $this->envoiMail;
    }

    public function getEnvoiReplyTo() {
        return $this->envoiReplyTo;
    }
	
	public function getEnvoiName() {
        return $this->envoiName;
    }
	
    public function getEnvoiHost() {
        return $this->envoiHost;
    }

    public function getEnvoiConnectionClass() {
        return $this->envoiConnectionClass;
    }

    public function getEnvoiPort() {
        return $this->envoiPort;
    }

    public function getEnvoiSsl() {
        return $this->envoiSsl;
    }

    public function getEnvoiUsername() {
        return $this->envoiUsername;
    }

    public function getEnvoiPassword() {
        return $this->envoiPassword;
    }
	
	 public function getMailAdministrateur() {
        return $this->mailAdministrateur;
    }

   

}
