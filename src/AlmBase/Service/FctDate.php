<?php
namespace AlmBase\Service;

use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\View\Model\ViewModel as ViewModel;

class FctDate implements ServiceManagerAwareInterface
{

    public function debutsem($date) {
        $d = $this -> gererFormatDate($date);
        $year = $d[0];
        $month = $d[1];
        $day = $d[2];
        
        $num_day      = date('w', mktime(0,0,0,$month,$day,$year));
        $premier_jour = mktime(0,0,0, $month,$day-(!$num_day?7:$num_day)+1,$year);
        $datedeb      = date('Y-m-d', $premier_jour);
        return $datedeb.' 00:00:00';
    }
    
    public function finsem($date) {
        $d = $this -> gererFormatDate($date);
        $year = $d[0];
        $month = $d[1];
        $day = $d[2];
        
        $num_day      = date('w', mktime(0,0,0,$month,$day,$year));
        $dernier_jour = mktime(0,0,0, $month,7-(!$num_day?7:$num_day)+$day,$year);
        $datedeb      = date('Y-m-d', $dernier_jour);
        return $datedeb.' 23:59:59';
    }
    
    private function gererFormatDate($date)
    {
        $d = explode(' ', $date); $d = explode('-', $d[0]);
        return $d;
    }
    
    /**
     * Set service manager instance
     *
     * @param ServiceManager $serviceManager
     * @return User
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }
}
