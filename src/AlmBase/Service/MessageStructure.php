<?php
namespace AlmBase\Service;

use Zend\Mail\Message;
use Zend\Mail\Transport\Sendmail as SendmailTransport;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\View\Model\ViewModel as ViewModel;
use Zend\Uri\Mailto;

class MessageStructure
{
    public function structureMail($contenu)
    { 
	    ob_start(); 
		?>
		<div style="width:650px; text-align:justify; margin:0 auto;">
        <p>
        <?= $contenu ?>
		</p>
        
        <p class="petit">PS: Ceci est un email automatique, merci de ne pas y répondre.</p>
        </p>
        </div>
		<? 
		$content = ob_get_contents ();
		ob_end_clean();
		return $content;
    }
   
    public function urlSite()
	{
	    $request = $serviceManager->get('Request');
	    $request->getUri();
	    $scheme = $uri->getScheme();
		$host = $uri->getHost();
		return $base = sprintf('%s://%s', $scheme, $host);
	}
}
