<?php
/*
 * NE PLUS UTILISER !!!!!!
 * 
 */


namespace AlmBase\Service;

use Zend\Mail\Message;
use Zend\Mail\Transport\Sendmail as SendmailTransport;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\View\Model\ViewModel as ViewModel;
use Zend\Uri\Mailto;

class Mail extends \Zend\Mail\Message implements ServiceManagerAwareInterface
{
    protected $html;
    protected $txt;
    
    public function setHtml($html){
        $this -> html = $html; 
    }
    
    public function setTexte($txt){
        $this -> txt = $txt; 
    }
    
    
    public function envoyerMail()
    {
       $options = $this->serviceManager->get('almBase_mail_options');
          
       // $this -> setEncoding("UTF-8");
        $this -> setSender($options->getEnvoiMail()); 
        $this -> addFrom($options->getEnvoiMail(), $options->getEnvoiFromName() );
       // $this -> addReplyTo($options->getEnvoiReplyTo());
        		
		$htmlPart = new MimePart(preg_replace("/(\r\n|\n|\r)/", " ", $this -> html));
        $htmlPart->type = \Zend\Mime\Mime::TYPE_HTML;
		$htmlPart->disposition = \Zend\Mime\Mime::DISPOSITION_INLINE;
		$htmlPart->encoding = \Zend\Mime\Mime::ENCODING_QUOTEDPRINTABLE;
		$htmlPart->charset = 'UTF-8';
	 
		$textPart = new MimePart(preg_replace("/(\r\n|\n|\r)/", " ", $this -> txt));
		$textPart->type = "text/plain";
	 
		$body = new MimeMessage();
		$body->setParts(array($textPart, $htmlPart));
		
     	$this->setEncoding("UTF-8");
    	$this->setBody($body);
    	$this->getHeaders()->get('content-type')->setType('multipart/alternative');
  
    	if($options->getType() == 'smtp') {
    	
    		$arrayConf = array();
    		if($options->getEnvoiName() != '') $arrayConf['name'] 					= $options->getEnvoiName();
    		if($options->getEnvoiHost() != '') $arrayConf['host'] 					= $options->getEnvoiHost();
    		if($options->getEnvoiConnectionClass() != '') $arrayConf['connection_class'] = $options->getEnvoiConnectionClass();
    		if($options->getEnvoiPort() != '') $arrayConf['port'] = $options->getEnvoiPort();
    		$arrayConf['connection_config']['username'] = $options->getEnvoiUsername();
    		$arrayConf['connection_config']['password'] = $options->getEnvoiPassword();
    		if($options->getEnvoiSsl() != '') $arrayConf['connection_config']['ssl'] = $options->getEnvoiSsl();
    		
    		$config = $this->serviceManager->get('Config');
			
		    if($config['app']['mode'] != 'prod'){
			    $this->setTo($config['app']['mailtest']); 
			    $this->setBcc(array());
			    $this->setCc(array());
		    }
					
			$transport = new SmtpTransport();
			$smtpOptions   = new SmtpOptions($arrayConf);
			$transport->setOptions($smtpOptions);
			
			
			$transport->send($this);
			
    	} else if($options->getType() == 'mailto') {
    	    $transport = new SendmailTransport();
    	    $transport->send($this);
    	    
    	}
    }
    
    /**
     * Set service manager instance
     *
     * @param ServiceManager $serviceManager
     * @return User
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }
}
