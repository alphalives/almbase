<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * @package   Zend_Form
 */

namespace AlmBase\Form\View\Helper;

use Zend\Form\ElementInterface;
use Zend\Form\Exception;
use Zend\Form\View\Helper\AbstractHelper;

use Zend\Form\View\Helper\FormRow;
use Zend\Form\View\Helper\FormLabel;
use Zend\Form\View\Helper\FormElement;
use Zend\Form\View\Helper\FormElementErrors;



/**
 * @category   Zend
 * @package    Zend_Form
 * @subpackage View
 */
class Select extends FormRow
{

    /**
     * Utility form helper that renders a label (if it exists), an element and errors
     *
     * @param ElementInterface $element
     * @return string
     * @throws \Zend\Form\Exception\DomainException
     */
    public function render(ElementInterface $element)
    {
		$value = $element->getValue();
		
		
		$elementErrorsHelper = $this->getElementErrorsHelper();
        $elementErrors   = $elementErrorsHelper->render($element);
        $options = $element->getAttribute('options');
		
		$display = $element->getAttribute('display');
		ob_start();
		?>
		
     
        
        <div class="control-group">
   		<label class="control-label" for="<?= $element->getName() ?>"><?= $element->getLabel() ?></label>
   
   		<div class="controls">
   			
   			<select id="<?= $element->getName() ?>" name="<?= $element->getName() ?>" data-placeholder=""  >
            <? foreach($options as $key => $option){ ?>
					<? if($element->getValue() == $key) $selected = 'selected="selected"'; else $selected = ""; ?>
					<option value="<?= $key ?>" <?= $selected ?>> <?= $option ?></option>
				<? } ?>
            </select>
   			<span class="help-inline"><?= $element->getAttribute('description') ?></span>
   		</div>
   		
   	</div>
        
        
        
		<? 
		$m = $element->getMessages();
		
		foreach($m as $key => $var){
			$m[$key] = 1;
		}
		
		 
	if ($elementErrors != '' || is_array($m)) { ?>
      <div id="formError_<?= $element->getName() ?>" class="contenuErreur"><span class="icoCroix"></span>
	  <? 
	 
	
	 
	  if(isset($m['recordFound'])){ ?>
             Ce mail a d�j� �t� utilis� pour cr�er un compte. 
             <a href="<?php echo $this -> view->url('zfcuser/login',array()); ?>?redirect=<?php echo $this -> view->url('zfcuser/loginFermerPopup'); ?>?redirect=<?= urlencode(base64_encode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] )) ?>" class="fancyboxLogin" data-fancybox-type="iframe">Connectez-vous � votre compte</a>. Si vous avez oubli� votre mot de passe merci de cliquer
             <a href="<?= $this->view->url('zfcuser/oubliPassword') ?>" class="fancyboxLogin" data-fancybox-type="iframe">ici</a>  
 	
	  <? }elseif (isset($m['mailNotValidate']) ) { ?>
	  		Vous n'avez pas valid� votre compte. Merci de cliquer sur le lien que vous avez re�u par mail.
	  
	  <? }elseif (isset($m['compteBloque'] )) { ?>
	  		Votre compte a �t� suspendu, si vous souhaitez le r�activer vous pouvez nous envoyer un mail � l'adresse : <a href="mailto:contact@nouvelle-annonce.com">contact@nouvelle-annonce.com</a> .
	  

	  <?  }elseif ($elementErrors != '') { ?><?= $elementErrors; ?><? } ?>
      
      </div>
	  <? } ?>
		<?
		$markup = ob_get_contents() ;
		ob_end_clean() ;
		
		return $markup;
    }

}
