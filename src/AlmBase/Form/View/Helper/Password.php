<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * @package   Zend_Form
 */

namespace AlmBase\Form\View\Helper;

use Zend\Form\ElementInterface;
use Zend\Form\Exception;
use Zend\Form\View\Helper\AbstractHelper;

use Zend\Form\View\Helper\FormRow;
use Zend\Form\View\Helper\FormLabel;
use Zend\Form\View\Helper\FormElement;
use Zend\Form\View\Helper\FormElementErrors;



/**
 * @category   Zend
 * @package    Zend_Form
 * @subpackage View
 */
class Password extends FormRow
{

    /**
     * Utility form helper that renders a label (if it exists), an element and errors
     *
     * @param ElementInterface $element
     * @return string
     * @throws \Zend\Form\Exception\DomainException
     */
    public function render(ElementInterface $element)
    {
		$value = $element->getValue();
		
		
		$elementErrorsHelper = $this->getElementErrorsHelper();
        $elementErrors   = $elementErrorsHelper->render($element);
		
		$display = $element->getAttribute('display');
		ob_start();
		?>
		
        
        <div class="control-group">
   		<label class="control-label" for="<?= $element->getName() ?>"><?= $element->getLabel() ?></label>
   
   		<div class="controls">
   			<input type="password" id="<?= $element->getName() ?>" name="<?= $element->getName() ?>" value="<?php echo $value; ?>" />
   			<span class="help-inline"><?= $element->getAttribute('description') ?></span>
   		</div>
   	</div>
        
        
       
		<?
		$markup = ob_get_contents() ;
		ob_end_clean() ;
		
		return $markup;
    }

}
