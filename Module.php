<?php
/**
 * Alphalives AlmBase 
 * AlmBase
 *
 */
namespace AlmBase;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Session\Config\SessionConfig;
use Zend\Session\SessionManager;
use Zend\Session\Container;
use Zend\EventManager\EventInterface;
use Zend\Log\Logger;

class Module implements AutoloaderProviderInterface
{

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__),
                )
            )
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $e)
    {
        
        
        
        // You may not need to do this if you're doing it elsewhere in your
        // application
        $app = $e->getApplication();
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $sm = $e->getApplication()->getServiceManager();
        $sharedManager = $eventManager->getSharedManager();
        
        $sessionManager = new SessionManager();
        $sessionManager->start();
        Container::setDefaultManager($sessionManager);
        
        date_default_timezone_set('Europe/Paris');
        setlocale(LC_TIME, "fr_FR");
        
        /*
         * Gestion des erreurs
         */
        if(! is_dir (__DIR__ . '/../../../data/log/')) mkdir(__DIR__ . '/../../../data/log/');
        $config = $sm->get('Config');
        $afficheError = (isset($config["app"]["mode"]) && $config["app"]["mode"] == 'dev') ? true : false;
        ini_set('display_errors', $afficheError);
        ini_set('display_startup_errors', $afficheError);
        
         //$sm->get('LoggerLogError')->registerErrorHandler( $sm->get('LoggerLogError'));
        $logger = $sm->get('LoggerLogError');
        
         
         
         if ($afficheError == false) {
           set_error_handler(function ($errno, $errstr, $errfile, $errline) use ($logger)
             {
                 $logger->log(\Zend\Log\Logger::ERR, $errno.' '.$errstr.' FILE : '.$errfile.' '.$errline, array(
                     'REMOTE_ADDR' => $_SERVER['REMOTE_ADDR'],
                     'URL' => $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
                     'HTTP_REFERER' => (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : '',
                     'HTTP_USER_AGENT' => $_SERVER['HTTP_USER_AGENT']
                 ));
                 
                 
                 
                 throw new \Exception($errstr . ' ' . $errfile . ' ' . $errline, $errno);
             });
         }
       
         ini_set('memory_limit', -1);
         
        $eventManager->attach(\Zend\Mvc\MvcEvent::EVENT_DISPATCH_ERROR, array(
            $this,
            'onDispatchError'
        ), 100);
        
        /*
         * Log
         */
        $sm->get('Logger')->log(\Zend\Log\Logger::INFO, 'INFO', array(
            'REMOTE_ADDR' => $_SERVER['REMOTE_ADDR'],
            'URL' => $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
            'HTTP_REFERER' => (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : '',
            'HTTP_USER_AGENT' => $_SERVER['HTTP_USER_AGENT']
        ));
        
        /*
         * Minify html
        */
        $app->getEventManager()->attach('finish', array($this, 'minifyHtml'), 100);
    }

    /**
     * Gestion des ereurs
     *
     * @param \Zend\Mvc\MvcEvent $e            
     */
    function onDispatchError(\Zend\Mvc\MvcEvent $e)
    {
       
        
        $sm = $e->getApplication()->getServiceManager();
        
        $vm = $e->getViewModel();
        $vm->setTemplate('layout/error');
        
        
        if ($e->getParam('exception')) {
            $ex = $e->getParam('exception');
            do {
                $sm->get('LoggerLogError')->log(\Zend\Log\Logger::ERR, $ex->getMessage(), array(
                    'REMOTE_ADDR' => $_SERVER['REMOTE_ADDR'],
                    'URL' => $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
                    'HTTP_REFERER' => (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : '',
                    'HTTP_USER_AGENT' => $_SERVER['HTTP_USER_AGENT']
                ));
            } while ($ex = $ex->getPrevious());
        }
    }
    
    public function minifyHtml($e)
    {
        $request        = $e->getRequest();
        $serviceManager = $e->getApplication()->getServiceManager();
        $assetManager   = $serviceManager->get('AssetManager\Service\AssetManager');
        if ($assetManager->resolvesToAsset($request)) {
            return;
        }
        
        $sm = $e->getApplication()->getServiceManager();
        $compressor = $sm -> get('almBase_htmlCompressor');
        $response = $e->getResponse();
        $content = $response->getBody();
        $content = $compressor -> html_compress($content, array('c'=>true,));
        $response->setContent($content);
    }

    public function getViewHelperConfig()
    {
        return array(
            'invokables' => array(
                
                'AlmBaseFormHidden' => 'AlmBase\Form\View\Helper\Hidden',
                'AlmBaseFormPassword' => 'AlmBase\Form\View\Helper\Password',
                'AlmBaseFormSelect' => 'AlmBase\Form\View\Helper\Select',
                'AlmBaseFormSelectChosen' => 'AlmBase\Form\View\Helper\SelectChosen',
                'AlmBaseFormText' => 'AlmBase\Form\View\Helper\Text',
                'AlmBaseFormTime' => 'AlmBase\Form\View\Helper\Time', 
				
				'formelement' => 'AlmBase\Form\View\Helper\FormElement', 
				'almbaseformdate' => 'AlmBase\Form\View\Helper\Date', 
            ),
			
        );
		
    }
	

    public function getServiceConfig()
    {
        return array(
            
            
            
            'factories' => array(
                'LoggerLogError' => function ($sm)
                {
                    $logger = new \Zend\Log\Logger();
                    $writer = new \Zend\Log\Writer\Stream(__DIR__ . '/../../../data/log/' . date('Y-m-d') . '-error.log');
                    $logger->addWriter($writer);
                    
                    return $logger;
                },
                
                'LoggerFirePhp' => function ($sm)
                {
                    $logger = new \Zend\Log\Logger();
                    $writer = new \Zend\Log\Writer\FirePhp();
                    $logger->addWriter($writer);
                    return $logger;
                },
                
                'Logger' => function ($sm)
                {
                    $logger = new \Zend\Log\Logger();
                    $writer = new \Zend\Log\Writer\Stream(__DIR__ . '/../../../data/log/' . date('Y-m-d') . '-log.log');
                    $logger->addWriter($writer);
                    return $logger;
                },
                
                'almBase_mail' =>  function ($sm) {
                    $return = new \AlmBase\Service\Mail();
                    return $return;
                },
                
                'almBase_mail_options' => function ($sm) {
                    $config = $sm->get('Config');
                    return new Options\MailOptions(isset($config['almbase-mail']) ? $config['almbase-mail'] : array());
                },
                
                'almBase_htmlCompressor' =>  function ($sm) {
                    $return = new \AlmBase\Service\HtmlCompressor();
                    return $return;
                },
                
                'fct_date' =>  function ($sm) {
                    $return = new \AlmBase\Service\FctDate();
                    return $return;
                },
                
                'almBase_messageStructure' =>  function ($sm) {
                    $return = new \AlmBase\Service\MessageStructure();
                    return $return;
                },
            )
        )
        ;
    }
}
