<?php
return array(
    
   'session' => array(
        'config' => array(
            'class' => 'Zend\Session\Config\SessionConfig',
            'options' => array(
                'name' => 'myapp',
            ),
        ),
        'storage' => 'Zend\Session\Storage\SessionArrayStorage',
        'validators' => array(
            array(
                'Zend\Session\Validator\RemoteAddr',
                'Zend\Session\Validator\HttpUserAgent',
            ),
        ),
    ),
	
	'asset_manager' => array(
         'resolver_configs' => array(
              'paths' => array(
                __DIR__ . '/../public',
              ),
         ),
      ),
    
    'view_manager' => array(
        'display_not_found_reason' => false,
        'display_exceptions'       => false,
        'template_map' => array(
            'layout/error'           => __DIR__ . '/../view/layout/error.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
   
);
